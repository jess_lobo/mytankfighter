﻿using UnityEngine;
using System.Collections;

public class moveChopper : MonoBehaviour {
	public float chopperMoveSpeed;
	// Use this for initialization
	void Start () 
	{

	}
	void OnBecameInvisible () 
	{
		this.gameObject.SetActive(false);
	}
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.up*chopperMoveSpeed*Time.deltaTime);
	}
}
