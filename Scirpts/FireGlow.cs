﻿using UnityEngine;
using System.Collections;

public class FireGlow : MonoBehaviour 
{
	public Sprite afterPress;
	public Sprite beforePress;
	public bool m_isPressed;
	Sprite sprite;
	// Use this for initialization
	void Start () 
	{	
		gameObject.GetComponent<SpriteRenderer> ().sprite = afterPress;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_isPressed)
		{
			gameObject.GetComponent<SpriteRenderer> ().sprite=afterPress;
			m_isPressed=false;
		}
		else{
			gameObject.GetComponent<SpriteRenderer> ().sprite=beforePress;
		}

	}


}
