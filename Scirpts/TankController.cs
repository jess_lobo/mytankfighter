﻿using UnityEngine;
using System.Collections;

public class TankController : MonoBehaviour {
	public Vector3 moveDirection= Vector3.zero;
	public float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (moveDirection *  speed*Time.deltaTime,Space.World);
		//transform.position = new Vector3(Mathf.Clamp(transform.position.x,-9.5f,9.5f),Mathf.Clamp(transform.position.y,-5.5f,5.5f),0);
	}
}
