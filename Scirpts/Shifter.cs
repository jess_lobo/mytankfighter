﻿using UnityEngine;
using System.Collections;

public class Shifter : MonoBehaviour 
{
	public GEAR shiftTo;
	public float shiftSpeed;
	public Transform[] shiftPositions;

	private Vector3 destination;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		destination = shiftPositions [(int)shiftTo].position;
		destination = new Vector3 (destination.x, destination.y, -1);
		transform.position = Vector3.Lerp (transform.position, destination, 0.1f* shiftSpeed);
	}
}
