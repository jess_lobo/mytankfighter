﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Way Point Edge
/// </summary>
[Serializable]
public class WayPointEdge
{
	/// <summary>
	/// Defines the direction of the Edge connecting to the next node
	/// </summary>
	public Vector3 canGo;
	/// <summary>
	/// Cost of the edge to travell 
	/// </summary>
	public float Cost;
	/// <summary>
	/// Waypoint to which the edge connects to.
	/// </summary>
	public WayPoint next;

	/// <summary>
	/// Initializes a new instance of the <see cref="WayPointEdge"/> class.
	/// </summary>
	/// <param name="direction">Direction.</param>
	/// <param name="weight">Weight.</param>
	/// <param name="nextOne">Adjuscent waypoint</param>
	/// </summary>
	public WayPointEdge(Vector3 direction,float weight, WayPoint nextOne)
	{
		this.canGo = direction;
		this.Cost = weight;
		this.next = nextOne;
	}
};


public class WayPoint : MonoBehaviour 
{
	/// <summary>
	/// The way point edges list max upto 4.
	/// </summary>
	public List<WayPointEdge> wayPointEdges;
//	public float h_heurestic;
	/// <summary>
	/// Used while calculating the shortest path by A* algorithm 
	/// </summary>
	public WayPoint parent;
	/// <summary>
	/// The g_movement cost used by A* algorithm.
	/// </summary>
	public float g_movementCost=0;
	/// <summary>
	/// The way point manager instance.
	/// </summary>
	WayPointManager wayPointManager;

	void Awake()
	{
		//Cache the wayPoint Manager and check for neighbouring wayPoints.
		wayPointManager = this.transform.parent.GetComponent<WayPointManager> ();
		//Check for the hit in Up direction of the wayPoint
	 	checkHit (new Vector2 (transform.position.x, transform.position.y + transform.localScale.y), Vector2.up);
		//Check for the hit in Down direction of the wayPoint
		checkHit (new Vector2 (transform.position.x, transform.position.y - transform.localScale.y), -Vector2.up);
		//Check for the hit in Right direction of the wayPoint
		checkHit (new Vector2 (transform.position.x + transform.localScale.x, transform.position.y), Vector2.right);
		//Check for the hit in Left direction of the wayPoint
		checkHit (new Vector2 (transform.position.x - transform.localScale.x, transform.position.y), -Vector2.right);

	}

	/// <summary>
	/// Checks the hit for finding the neighbouring wayPoints and stores them in the wayPointEdges list
	/// </summary>
	/// <param name="pos">Current way point position</param>
	/// <param name="direction">Direction along which to cast the ray</param>
	void checkHit(Vector2 pos,Vector2 direction)
	{
		RaycastHit2D hit = Physics2D.Raycast (pos, direction, 10,LayerMask.GetMask("WayPoint"));
		if (hit.collider != null) 
		{
			if(hit.transform!=transform)
			{
				if(hit.collider.tag=="Way Point")
				{
					float cost= Vector3.Distance(transform.position,hit.transform.position);
					wayPointEdges.Add(new WayPointEdge(direction,cost,hit.transform.GetComponent<WayPoint>()));
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		// If player enter the trigger set current way Point as destination and 
		// tell way point manager to Calculate the path
		if (other.tag == "Player") 
		{
			wayPointManager.destination = this.transform.GetComponent<WayPoint>();
			wayPointManager.IsDestinationChanged=true;
		}
		// If AI Enters the trigger se the current way point as the source for the path calculation
		else if (other.tag == "TankAI") 
		{
			wayPointManager.source = this.transform.GetComponent<WayPoint>();
		}

	}
}
