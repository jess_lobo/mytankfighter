﻿using UnityEngine;
using System.Collections;

public class RotateBase : MonoBehaviour {
	public Transform shifter;
	public float rotateSpeed;
	private Shifter shifterScript;
	// Use this for initialization
	void Start () {
		shifterScript = shifter.gameObject.GetComponent<Shifter> ();
	}
	
	// Update is called once per frame
	void Update () {
		switch (shifterScript.shiftTo) {
		case GEAR.Up:
		case GEAR.Right: this.transform.Rotate(Vector3.forward * Time.deltaTime * -rotateSpeed);
			break;
		case GEAR.Down:
		case GEAR.Left: this.transform.Rotate(Vector3.forward * Time.deltaTime * rotateSpeed);
			break;
		}
	}
}
