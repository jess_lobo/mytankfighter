﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WayPointManager : MonoBehaviour 
{
	public bool IsDestinationChanged;
	public WayPoint destination;
	public WayPoint source;
	// Reference of the AI Tank : To the movement of the TankAI according the path calculated
	public Transform TankAI;

	//Tank AI Script Reference
	private TankAIController tankAIScript;
	private WayPoint PreviousDestination;


	// Use this for initialization
	void Start () 
	{
		//Cache the tank AI script reference
		tankAIScript = TankAI.GetComponent<TankAIController> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//If new destination is set by the player and which is not equal to the previous destination
		//Then Calculate Path
		if (IsDestinationChanged && destination!=PreviousDestination) 
		{
			PreviousDestination=destination;
			CalculateThePath();
			IsDestinationChanged=false;
		}
	}

	/// <summary>
	/// Calculates the path.
	/// ******* A* Algorithm Implemention ********
	/// Author : Jesudas Lobo
	/// </summary>
	void CalculateThePath()
	{
		//Define Open and closed list
		List <WayPoint> OpenList 	= new List<WayPoint>();
		List <WayPoint> ClosedList	= new List<WayPoint>();

		//As source is starting point of the path to be found, set parent of source to null
		source.parent 				= null;

		//Ad the source to the Open List
		OpenList.Add (source);

		WayPoint current ;

		//While open list count is 0 
		while (OpenList.Count>0) 
		{
			//Select the Best Node from the Open List 
			current = SelectBestFromOpenList(OpenList);

			//If the destination is found : Print the shortest path
			if(current == destination)
			{
				//Path to the destination is found
				//Now tell the AI to stop following previous path by clearing its traverse data
				tankAIScript.ToTraverse.Clear();

				//While the parent is not null, since source parent is null, traverse back
				while(current.parent!=null)
				{
					//fetch the current node and traverse back by pointing to its parent
					tankAIScript.ToTraverse.Add(current);
					current=current.parent;
				}
				//tankAIScript.ToTraverse.Add(current);
				//Inform AI that new path traverse list is ready to be followed
				tankAIScript.isNewPathFound=true;
				break;
			}
			else
			{
				//else the add the current best selected node to the closed list and remove from open list
				ClosedList.Add(current);
				OpenList.Remove(current);

				//Get all successors of the current node 
				List<WayPoint> waypoints= getSuccessorOf(current,ClosedList);

				//For each node in the successors 
				foreach(WayPoint neighbour in waypoints)
				{
					WayPoint NodeOpen=null;
					//If open list contains the neighbour
					if(OpenList.Contains(neighbour))
						NodeOpen = OpenList[OpenList.IndexOf(neighbour)];

					//and cost of traverse path to the destination through node present in the Open list less 
					//then continue the loop
					if(NodeOpen != null && GetTotalCost(neighbour) >GetTotalCost(NodeOpen))
						continue;

					//if closed list contains the neighbour
					if(ClosedList.Contains(neighbour))
					{
						print ("Don't remove this");
						continue;
					}
					// At last Add the neighbour to the openlist satisfying all the above condition.
					OpenList.Remove(NodeOpen);
					OpenList.Add(neighbour);
				}
			}
		}
	}


	/// <summary>
	/// Selects the best node from the open list.
	/// </summary>
	/// <returns>The best node from open list.</returns>
	/// <param name="OpenList">Open list.</param>
	WayPoint SelectBestFromOpenList(List<WayPoint> OpenList)
	{
		float previousCost = 999;
		float totalCost = 0;
		WayPoint bestNode=null;

		//for each item in the Open List, Select node having the low cost
		foreach(WayPoint item in OpenList)
		{
			//Get the the cost for current item to the destination
			totalCost = GetTotalCost(item);
			if(totalCost<previousCost)
			{
				previousCost =totalCost;
				bestNode=item;
			}
		}
		return bestNode;
	}


	/// <summary>
	/// Gets the successor of current node.
	/// </summary>
	/// <returns>The successor of current node.</returns>
	/// <param name="current">Object.</param>
	/// <param name="ClosedList">Closed list.</param>
	List<WayPoint> getSuccessorOf(WayPoint current,List<WayPoint> ClosedList)
	{
		List<WayPoint> successors= new List<WayPoint>();
		List<WayPointEdge> wayPointsEdges = current.wayPointEdges;

		//For each edge in WayPointEdges of the current node
		foreach (WayPointEdge wayPointEdge in wayPointsEdges) 
		{
			//if closedList contains the node connected by the edge from current node then continue loop
			if(ClosedList.Contains(wayPointEdge.next))
			   continue;
			//Set the parent for the node reachable from current as current wayPoint
			wayPointEdge.next.parent = current;

			//Set the edge weight from current to reachable node, to the g_movementCost of the rechable node
			wayPointEdge.next.g_movementCost = wayPointEdge.Cost;

			//store all the valid reachable node for returning to the calling function
			successors.Add(wayPointEdge.next);
		}
		return successors;
	}


	/// <summary>
	/// Gets the total cost from current way point to the destination.
	/// </summary>
	/// <returns>The total cost.</returns>
	/// <param name="neighbour">neighbour.</param>
	float GetTotalCost(WayPoint neighbour)
	{
		//Return cost of edge betn current and neighbour waypoint + the distance from neighbour to the destination
		return (neighbour.g_movementCost +Vector3.Distance(neighbour.transform.position,destination.transform.position));
	}
}
