﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class TankAIController : MonoBehaviour 
{
	//Start position for the AI
	public Transform StartPosition;
	//Stack of WayPoint List to traverse
	public List<WayPoint> ToTraverse=new List<WayPoint>();
	public bool isNewPathFound;
	public bool canFollow;
	public bool isShooting;
	public float speed;
	private float nextFire;

	private Transform bulletPos;
	private WayPoint nextNodeInThePath;
	// Use this for initialization
	void Start ()
	{
		transform.position = StartPosition.position;
		bulletPos = this.transform.GetComponentInChildren<Transform> ().Find ("Bullet Position");
	}
	
	// Update is called once per frame
	void Update () 
	{
		bulletPos.renderer.enabled = false;
		checkPlayerIsInShootingRange ();
		if (isNewPathFound && ToTraverse.Count>0) 
		{
			nextNodeInThePath=ToTraverse[ToTraverse.Count-1];
			ToTraverse.RemoveAt(ToTraverse.Count-1);
			canFollow=true;
			isNewPathFound=false;
		}
		if (canFollow && !isShooting) 
		{
			Follow();
		}
		if (isShooting) 
		{
			if(Time.time>nextFire)
			{
				bulletPos.renderer.enabled=true;
				nextFire = Time.time +2f;
				audio.Play();
				GameObject obj = ObjectPoolerScript.current.GetPooledObject();
				obj.transform.position=bulletPos.position;
				obj.transform.rotation=bulletPos.rotation;
				obj.transform.GetComponent<BulletScript>().tank = this.transform;
				obj.SetActive(true);
			}
			isShooting=false;
		}
	}

	void Follow()
	{
		Vector3 dir = nextNodeInThePath.transform.position - transform.position;
		if (dir != Vector3.zero) 
		{
			float angle = Mathf.Atan2(-dir.x, dir.y) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}
		transform.position = Vector3.MoveTowards (transform.position, nextNodeInThePath.transform.position, Time.deltaTime * speed);
		if( Vector3.Distance(transform.position,nextNodeInThePath.transform.position)<0.1f)
		{
			if(ToTraverse.Count==0)
			{
				canFollow=false;
			}else{
				nextNodeInThePath=ToTraverse[ToTraverse.Count-1];
				ToTraverse.RemoveAt(ToTraverse.Count-1);
			}
		}
	}

	void checkPlayerIsInShootingRange()
	{
		checkHit (new Vector2 (transform.position.x, transform.position.y + transform.localScale.y), Vector2.up);
		checkHit (new Vector2 (transform.position.x, transform.position.y - transform.localScale.y), -Vector2.up);
		checkHit (new Vector2 (transform.position.x + transform.localScale.x, transform.position.y), Vector2.right);
		checkHit (new Vector2 (transform.position.x - transform.localScale.y, transform.position.y), -Vector2.right);
	}
	void checkHit(Vector2 pos,Vector2 direction)
	{
		RaycastHit2D hit = Physics2D.Raycast(pos, direction,3);
		Debug.DrawRay (pos, direction * 3);
		if (hit.collider != null) 
		{
			if(hit.transform!=transform)
			{
				if(hit.collider.tag=="Player")
				{
					//Rotate Towards Player and Shoot
					Vector3 dir = hit.transform.position - transform.position;
					if (dir != Vector3.zero) 
					{
						float angle = Mathf.Atan2(-dir.x, dir.y) * Mathf.Rad2Deg;
						transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
					}
					isShooting=true;
				}
			}
		}
	}
}
