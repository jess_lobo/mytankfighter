﻿using UnityEngine;
using System.Collections;

public enum GEAR 
{
	Neutral,Up,Down,Right,Left
};

[RequireComponent(typeof(AudioSource))]
public class ShiftController : MonoBehaviour 
{
	public GameObject player;
	public float speed;
	public Camera cam;
	
	private Shifter shifterScript;
	private TankController playerController;
	private Transform bulletPos;
	private float nextFire;
	// Use this for initialization
	void Start () 
	{
		shifterScript = transform.GetComponentInChildren<Transform> ().Find ("Shifter").GetComponent<Shifter>();
		playerController = player.GetComponent<TankController> ();
		bulletPos = player.transform.GetComponentInChildren<Transform> ().Find ("Bullet Position");
	}

	// Update is called once per frame
	void Update () 
	{
		bulletPos.renderer.enabled = false;
		#if UNITY_EDITOR
		if (Input.GetMouseButton(0)) {
			Ray ray= cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray.origin,ray.direction,out hit,30))
			{
				if(hit.transform.tag=="Gear")
				{
					if(hit.transform.name=="Neutral")
					{
						shifterScript.shiftTo=GEAR.Neutral;
						playerController.moveDirection=Vector3.zero;
					}
					if(hit.transform.name=="UpLock")
					{
						shifterScript.shiftTo=GEAR.Up;
						playerController.moveDirection=Vector3.up;
						player.transform.localEulerAngles= Vector3.zero;
					}
					if(hit.transform.name=="DownLock")
					{
						shifterScript.shiftTo=GEAR.Down;
						playerController.moveDirection=Vector3.down;
						player.transform.localEulerAngles= Vector3.forward*180;
					}
					if(hit.transform.name=="RightLock")
					{
						shifterScript.shiftTo=GEAR.Right;
						playerController.moveDirection=Vector3.right;
						player.transform.localEulerAngles= Vector3.forward*-90;
					}
					if(hit.transform.name=="LeftLock")
					{
						shifterScript.shiftTo=GEAR.Left;
						playerController.moveDirection=Vector3.left;
						player.transform.localEulerAngles= Vector3.forward*90;
					}

				}

				if(hit.transform.name=="Fire")
				{
					bulletPos.renderer.enabled=true;
					hit.collider.gameObject.GetComponent<FireGlow>().m_isPressed = true;
					if(Time.time>nextFire)
					{
						nextFire = Time.time + 2f;
						audio.Play();
						GameObject obj = ObjectPoolerScript.current.GetPooledObject();
						obj.transform.position=bulletPos.position;
						obj.transform.rotation=bulletPos.rotation;
						obj.transform.GetComponent<BulletScript>().tank = player.transform;
						obj.SetActive(true);
					}
				}
			}
		}
		else
		{
			shifterScript.shiftTo=GEAR.Neutral;
			playerController.moveDirection=Vector3.zero;
		}
#else
	if (Input.touchCount>0) {
		foreach(Touch touch in Input.touches){
			Ray ray= cam.ScreenPointToRay(touch.position);
			RaycastHit hit;
			if(Physics.Raycast(ray.origin,ray.direction,out hit,30)){
				if(hit.transform.tag=="Gear"){
					if(hit.transform.name=="Neutral"){
						shifterScript.shiftTo=GEAR.Neutral;
						playerController.moveDirection=Vector3.zero;
					}
					if(hit.transform.name=="UpLock"){
						shifterScript.shiftTo=GEAR.Up;
						playerController.moveDirection=Vector3.up;
						player.transform.localEulerAngles= Vector3.zero;
					}
					if(hit.transform.name=="DownLock"){
						shifterScript.shiftTo=GEAR.Down;
						playerController.moveDirection=Vector3.down;
						player.transform.localEulerAngles= Vector3.forward*180;
					}
					if(hit.transform.name=="RightLock"){
						shifterScript.shiftTo=GEAR.Right;
						playerController.moveDirection=Vector3.right;
						player.transform.localEulerAngles= Vector3.forward*-90;
					}
					if(hit.transform.name=="LeftLock"){
						shifterScript.shiftTo=GEAR.Left;
						playerController.moveDirection=Vector3.left;
						player.transform.localEulerAngles= Vector3.forward*90;
					}
					}
					if(hit.transform.name=="Fire"){
						bulletPos.renderer.enabled=true;
						hit.collider.gameObject.GetComponent<FireGlow>().m_isPressed = true;
						if(Time.time>nextFire){
							nextFire = Time.time + 2f;
							audio.Play();
							GameObject obj = ObjectPoolerScript.current.GetPooledObject();
							obj.transform.position=bulletPos.position;
							obj.transform.rotation=bulletPos.rotation;
							obj.transform.GetComponent<BulletScript>().tank = player.transform;
							obj.SetActive(true);
						}
					}
			}
		}
	}
	else
	{
		shifterScript.shiftTo=GEAR.Neutral;
		playerController.moveDirection=Vector3.zero;
	}
#endif
}

}
