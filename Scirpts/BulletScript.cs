﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	public Transform tank;
	public float bulletSpeed;
	// Use this for initialization

	void OnEnable() {
		Invoke ("Destroy", 1.0f);
	}
	void Destroy(){
		gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up*bulletSpeed*Time.deltaTime);
	}
	void OnDisable() {
		CancelInvoke ();
	}
	void OnTriggerEnter2D(Collider2D other){
		if(other.tag =="Boundary")
			Destroy ();
		if (other.tag == "Player" && other.transform != tank.transform) {
			Destroy ();	
		}
		if (other.tag == "TankAI" && other.transform != tank.transform) {
			Destroy ();	
		}
	}
}
